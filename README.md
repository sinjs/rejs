# ReJS, the JavaScript REPL

## Introduction

ReJS is a live JavaScript REPL without any ads, accounts or payments.
It works without needing to register or login and you can share code to your friends.

This is still in development so it may not work correctly, please report any bugs under the "Issues" section on GitLab.

## Running locally

```bash
# clone the repository
$ git clone https://gitlab.com/sinjs/rejs.git
$ cd rejs

# install dependencies
$ yarn install

# build the server
$ yarn build

# generate the static files for the frontend
$ yarn generate

# and then run the server
$ yarn start # you may need to run it as root to bind on port 80 on linux.
```

## Source code structure

The code is structured like this:

```bash
rejs
├── assets # assets, mostly codemirror stuff
│   ├── codemirror.js
│   └── ...
├── components # reusable components, mostly icons
│   ├── CloseIcon.vue
│   ├── SaveIcon.vue
│   └── ...
├── dist
│   └── ... # this directory is ignored by git, it will be created with "yarn generate"
├── layouts
│   └── default.vue # the default layout
├── nuxt.config.js # the nuxt configuration
├── package.json # package configuration
├── pages
│   └── index.vue # the index page, it contains all the logic for the frontend
├── README.md # what you are reading right now
├── server # the server directory, all the logic for the backend is contained in here
│   ├── index.ts
│   └── out
│       └── ... # this directory is ignored by git, it will be created with "yarn build"
├── static # static assets
│   └── ...
├── tailwind.config.js # the tailwindcss configuration
├── tsconfig.json # the typescript compiler config for the frontend
├── tsconfig.server.json # the typescript compiler config for the backend
└── yarn.lock # the lockfile
```
