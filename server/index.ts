import express from 'express'
import proxy from 'express-http-proxy'
import apiRouter from './api/router'

const app: express.Application = express()

app.disable('x-powered-by')

app.use('/api', apiRouter)
app.use('/', proxy('127.0.0.1:5000'))

app.listen(80, () => {
  // eslint-disable-next-line no-console
  console.log('listening on port 80')
})
