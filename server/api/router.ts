import express from 'express'
import bodyParser from 'body-parser'

import rCreateSession from './createSession'
import rUpdateValue from './updateValue'
import rGetValue from './getValue'

const router: express.Router = express.Router()

router.use(bodyParser.json())
router.use(bodyParser.urlencoded())

router.get('/test', (_req, res) => {
  res.send('lol')
})

router.get('/create-session', rCreateSession)
router.post('/update-value', rUpdateValue)
router.get('/get-value', rGetValue)

export default router
