import express from 'express'
import store from '../util/store'

function handleRequest(req: express.Request, res: express.Response) {
  res.contentType('application/json')
  if (!req.body.session || !req.body.token || !req.body.value) {
    res.status(400)
    return res.json({
      error: true,
      code: 400,
      data: null,
      message: 'Invalid or missing parameters.',
    })
  }

  if (!store.has(req.body.session)) {
    res.status(400)
    return res.json({
      error: true,
      code: 400,
      data: null,
      message: 'Invalid session uuid.',
    })
  }

  if (store.get(req.body.session).token !== req.body.token) {
    res.status(401)
    return res.json({
      error: true,
      code: 401,
      data: null,
      message: 'Unauthorized. The token is invalid.',
    })
  }

  if (store.get(req.body.session).status === 'CLOSED') {
    res.status(410)
    return res.json({
      error: true,
      code: 410,
      data: null,
      message: 'The session has been closed.',
    })
  }

  store.set(req.body.session, {
    status: 'RUNNING',
    value: req.body.value,
    token: req.body.token,
  })

  res.json(store.get(req.body.session))
}
export default handleRequest
