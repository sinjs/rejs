import express from 'express'
import store from '../util/store'

function handleRequest(req: express.Request, res: express.Response) {
  res.contentType('application/json')

  if (!req.query.uuid) {
    res.status(400)
    return res.json({
      error: true,
      code: 400,
      data: null,
      message: 'Missing uuid query parameter.',
    })
  }

  if (!store.has(req.query.uuid.toString())) {
    res.status(400)
    return res.json({
      error: true,
      code: 400,
      data: null,
      message: 'This session does not exist.',
    })
  }

  return res.json(store.get(req.query.uuid.toString()).value)
}

export default handleRequest
