import crypto from 'crypto'
import express from 'express'
import { v4 as uuid } from 'uuid'
import store from '../util/store'

function handleRequest(_req: express.Request, res: express.Response) {
  res.contentType('application/json')
  const sessionUuid = uuid()
  store.set(sessionUuid, {
    status: 'CREATED',
    value: null,
    token: crypto.randomBytes(16).toString('hex'),
  })
  res.json({
    error: false,
    code: 200,
    data: { uuid: sessionUuid, storeValue: store.get(sessionUuid) },
    message: null,
  })
}

export default handleRequest
