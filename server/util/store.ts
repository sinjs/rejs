interface StoreObject {
  [key: string]: any
}

class StoreInstance {
  constructor() {
    this._st = {}
  }

  private _st: StoreObject

  set(key: string, value: any): any {
    return (this._st[key] = value)
  }

  remove(key: string): boolean {
    return delete this._st[key]
  }

  get(key: string): any | undefined {
    return this._st[key]
  }

  has(key: string): boolean {
    return !!this._st[key]
  }

  map(): Map<string, any> {
    return new Map(Object.entries(this._st))
  }
}

export default new StoreInstance()
